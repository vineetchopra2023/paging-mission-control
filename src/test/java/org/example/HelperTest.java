package org.example;

import org.junit.jupiter.api.Test;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

/*
* Unit test for Helper class
*/
public class HelperTest {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

    Telemetry telemetry;
    Telemetry telemetry1;
    Telemetry telemetry2;
    Telemetry telemetry3;

    {
        try {
            telemetry = new Telemetry(sdf.parse("20180101 23:01:05.001"),1001,101,98,25,20,99.9,"TSTAT");
            telemetry1 = new Telemetry(sdf.parse("20180101 23:01:09.521"),1000,17,15,9,8,7.8,"BATT");
            telemetry2 = new Telemetry(sdf.parse("20180101 23:02:11.302"),1000,17,15,9,8,7.8,"BATT");
            telemetry3 = new Telemetry(sdf.parse("20180101 23:04:11.531"),1000,17,15,9,8,7.8,"BATT");
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    Alert alert = new Alert(1001,"test2","test3","20180101 23:04:11.531");


    @Test
    public void testInputTelemetryFile(){

        List<Telemetry> expectedList = new ArrayList<>();
        expectedList.add(telemetry);
        //Path path = Paths.get("data/sample_test_data.txt");
        List<Telemetry> actualList = Helper.readInputTelemetryFile("data/sample_test_data.txt");
        assertEquals(1,actualList.size());
        assertEquals(expectedList.get(0).getSatelliteId(), actualList.get(0).getSatelliteId());
    }

    @Test
    public void testAlertIsCreated(){
        List<Alert> expectedAlertList = new ArrayList<>();
        List<Alert> actualAlertList = new ArrayList<>();
        expectedAlertList.add(alert);
        actualAlertList = Helper.createAlert(1001,"test2","test3",actualAlertList);
        assertEquals(1,actualAlertList.size());
        assertEquals(expectedAlertList.get(0).getSatelliteId(), actualAlertList.get(0).getSatelliteId());
    }

    @Test
    public void testTimeInterval(){
        List<Telemetry> expectedList = new ArrayList<>();
        expectedList.add(telemetry);
        Date startDate = expectedList.stream().findFirst().get().getTimestamp();
        List<Telemetry> actualList = Helper.getListByDate(startDate,expectedList);
        assertEquals(1,actualList.size());
    }

    @Test
    public void testAlertBySatalliteID(){
        List<Alert> expectedAlertList = new ArrayList<>();
        List<Telemetry> telemetriesList = new ArrayList<>();
        telemetriesList.add(telemetry1);
        telemetriesList.add(telemetry2);
        telemetriesList.add(telemetry3);
        String expectedSeverity = "RED LOW";
        List<Alert> actualAlertList = Helper.getAlertBySatalliteID(1000,telemetriesList,"BATT",expectedAlertList);
        assertEquals(1,actualAlertList.size());
        assertEquals(expectedSeverity,actualAlertList.get(0).getSeverity());
    }

}
