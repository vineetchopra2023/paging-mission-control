package org.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 * This is a Helper class that performs the calculation to create a Alert message
 * It filters the complete Telemetry list by satellite ID and the component
 * Sort the filtered list so the first object has the start date to calculate the 5-minute time interval
 * Create a recursive call by making sure there are least 3 telemetry objects in the 5 minutes time frame.
 * If there are more than three it will make another call to filter by time frame. If not will move on..
 * Based on the Component it will check if an alert has to be created - filter the list to check if any condition is met
 * If condition is met an alert is created and added to Alert list to be displayed later
 */
public class Helper {

    /*
    * Filter the list by component and satelliteID. And then check within 5 minutes  time frame
    * if an alert is created
     */
    public static List<Alert> getAlertBySatalliteID(Integer satelliteID, List<Telemetry> inList, String component, List<Alert> alertList) {
        List<Telemetry> checkList = new ArrayList<>();
        String checkSeverity = "";
        List<Telemetry> satelliteIDList = inList.stream().filter(s -> s.getSatelliteId() == satelliteID && s.getComponent().equals(component)).collect(Collectors.toList());
        int satelliteIDListSize = satelliteIDList.size();
        // Sort the sataliteIDList by timestamp
        Collections.sort(satelliteIDList, Comparator.comparing(Telemetry::getTimestamp));
        //Start date is first date form the sorted list
        Date startDate = satelliteIDList.stream().findFirst().get().getTimestamp();
        //Call getListByDate recursively
        while (satelliteIDListSize > 0) {
            List<Telemetry> listByDateInterval = getListByDate(startDate, satelliteIDList);
            satelliteIDListSize = satelliteIDListSize - listByDateInterval.size();
            //Create Alert if only three more entry in Telemetry within the Time frame (5 minutes)
            if (listByDateInterval.size() > 2) {
                //New start date where replaced by the END date from previous time frame interval
                startDate = new Date(startDate.getTime() + TimeUnit.MINUTES.toMillis(5));
                if (component.equals("BATT")) {
                    checkSeverity = "RED LOW";
                    checkList = listByDateInterval.stream().filter(s -> s.getRawValue() < s.getRedLowLimit()).collect(Collectors.toList());
                } else if (component.equals("TSTAT")) {
                    checkSeverity = "RED HIGH";
                    checkList = listByDateInterval.stream().filter(s -> s.getRawValue() > s.getRedHighLimt()).collect(Collectors.toList());
                } else {
                    //do Nothing
                }

                if (checkList.size() > 0) {
                    //Create a new Alert and add it to Alert list to be displayed later
                    alertList = createAlert(satelliteID, checkSeverity, component,alertList);
                }
            }
        }
        return alertList;
    }

    /*
     * Filters the list with Time interval which takes Start date and the list for Telemetry for each satellite and component
     */

    public static List<Telemetry> getListByDate(Date startDate, List<Telemetry> inList) {
        Date endDate = new Date(startDate.getTime() + TimeUnit.MINUTES.toMillis(5));
        List<Telemetry> myList = inList.stream().filter(dates -> dates.getTimestamp().compareTo(startDate) >= 0 &&
                dates.getTimestamp().before(endDate)).collect(Collectors.toList());
        return myList;
    }

    /*
    * Create a new Alert and add it to Alert list to be displayed later
     */
    public static List<Alert> createAlert(int satelliteID, String checkSeverity, String component, List<Alert> alertList){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String date = sdf.format(new Date());
        Alert newAlert = new Alert(satelliteID, checkSeverity, component, date);
        alertList.add(newAlert);
        return alertList;
    }

    /*
    * Read the Telemetry input datafile, parse it by | delimiter
    * Each line is mapped to a Telemetry  object which is added to a list of Telemetry
    * Try and Catch is added to catch any exception while reading the Telemetry input file
     */
    public static List<Telemetry> readInputTelemetryFile(String arg) {
        Path path = Paths.get(arg);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        try (Stream<String> lines = Files.lines(path)) {
            List<Telemetry> data = lines.map(line -> {
                String[] arr = line.split("\\|");
                try {
                    return new Telemetry(
                            sdf.parse(arr[0]),
                            Integer.parseInt(arr[1]),
                            Integer.parseInt(arr[2]),
                            Integer.parseInt(arr[3]),
                            Integer.parseInt(arr[4]),
                            Integer.parseInt(arr[5]),
                            Double.parseDouble(arr[6]),
                            arr[7]);
                } catch (ParseException e) {
                    System.out.println("ERROR:"+ e);
                    throw new RuntimeException(e);
                }
            }).collect(Collectors.toList());
            return data;
        } catch (IOException e) {
            System.out.println("ERROR:"+ e);
            throw new RuntimeException(e);

        }

    }
}
