package org.example;

import java.io.Serializable;
import java.util.Date;

public class Telemetry implements Serializable {

    private static final long serialVersionUID = 1L;
    private Date timestamp;
    private int  satelliteId;
    private int  redHighLimt;
    private  int  yellowHighLimit;
    private int  yellowLowLimit;
    private int redLowLimit;
    private double rawValue;
    private String component;

    public Telemetry(Date timestamp, int satelliteId, int redHighLimt, int yellowHighLimit, int yellowLowLimit, int redLowLimit, double rawValue, String component) {
        this.timestamp = timestamp;
        this.satelliteId = satelliteId;
        this.redHighLimt = redHighLimt;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
        this.component = component;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(int satelliteId) {
        this.satelliteId = satelliteId;
    }

    public int getRedHighLimt() {
        return redHighLimt;
    }

    public void setRedHighLimt(int redHighLimt) {
        this.redHighLimt = redHighLimt;
    }

    public int getYellowHighLimit() {
        return yellowHighLimit;
    }

    public void setYellowHighLimit(int yellowHighLimit) {
        this.yellowHighLimit = yellowHighLimit;
    }

    public int getYellowLowLimit() {
        return yellowLowLimit;
    }

    public void setYellowLowLimit(int yellowLowLimit) {
        this.yellowLowLimit = yellowLowLimit;
    }

    public int getRedLowLimit() {
        return redLowLimit;
    }

    public void setRedLowLimit(int redLowLimit) {
        this.redLowLimit = redLowLimit;
    }

    public double getRawValue() {
        return rawValue;
    }

    public void setRawValue(double rawValue) {
        this.rawValue = rawValue;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }
}
