package org.example;

import com.google.gson.Gson;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/*
 * TO RUN on Windows cmd prompt gradlew run --args="data\extended_sample_data.txt"
 * TO RUN on IDEA Terminal - C:\...\paging-mission-control> ./gradlew run --args="data\extended_sample_data.txt"
 * Main class to start the Alert Application to create a RED HIGH and RED LOW Alert bases on condition:
 * If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
 * If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
 */

public class Main {
    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("Please provide Path to a ASCII text file.");
        } else if (args.length == 1) {
            // Read data from Telemetry input file
            List<Telemetry> data = Helper.readInputTelemetryFile(args[0]);

            //Initialize GSON to convert Telemetry list Object to JSON
            Gson gson = new Gson();

            // Initialize Alter List
            List<Alert> alertList =  new ArrayList<>();

            //Filter by satelliteId
            List<Integer> satelliteId = data.stream().map(Telemetry::getSatelliteId).distinct().collect(Collectors.toList());

            // Fore each satelliteId get the Alerts
            for (Integer id : satelliteId) {
                alertList = Helper.getAlertBySatalliteID(id,data,"BATT",alertList);
                alertList = Helper.getAlertBySatalliteID(id,data,"TSTAT",alertList);
            }

            // this method converts a list to JSON Array
            String jsonInString = gson.toJson(alertList);
            System.out.println(jsonInString);
        } else{
            System.out.println("Please provide Path to a ASCII text file in the first Argument.");
        }




    }
}